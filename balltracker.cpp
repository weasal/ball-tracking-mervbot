#include <limits>
#include "algorithms.h"
#include "clientprot.h"
#include "host.h"

/* Returns whether the ball collided during this update */
bool Host::updateBall(PBall* pb) {
	bool collided = false;
	Uint32 time = getTime();

	int updateticks = (time - pb->updatetime);

	while (updateticks > 0) {
		if (pb->xvel || pb->yvel) {
			int oldX = pb->x;
			int oldY = pb->y;
			pb->x += pb->xvel;
			pb->y += pb->yvel;

			int workfriction = pb->frictiontimer / 1000;
			pb->xvel = pb->xvel * workfriction / 1000;
			pb->yvel = pb->yvel * workfriction / 1000;

			//--- Check for collisions
			IntersectResult result = ballPathIntersect(oldX, oldY, pb->x, pb->y);
			IntersectResult cornerResult;
			if (result.intersected) {
				collided = true;

				// Need to test the reflection once too (this is necessary to handle when
				// the ball is shot into a corner: o> )
				logEvent("Ball Path Intersect (%s)", (result.dir == IntersectResult::Direction::Horizontal) ? "horizontal" : "vertical");
				if (result.dir == IntersectResult::Direction::Horizontal) {
					pb->xvel *= -1;
					pb->x += pb->xvel;

					// Note!: this code doesn't account for a ball with speed > tile size (16000) getting in gaps shaped like
					// below (i.e. double bounce in same direction): (assumption: no one is going to create maps like that)
					//		T   T
					//		T T T
					cornerResult = ballPathIntersect(static_cast<int>(result.ix), static_cast<int>(result.iy), pb->x, pb->y);
					if (cornerResult.intersected) { // Assume corner (dir is vertical)
						pb->yvel *= -1;
						pb->y += pb->yvel;
					}
				}
				else {
					pb->yvel *= -1;
					pb->y += pb->yvel;

					cornerResult = ballPathIntersect(static_cast<int>(result.ix), static_cast<int>(result.iy), pb->x, pb->y);
					if (cornerResult.intersected) {
						pb->xvel *= -1;
						pb->x += pb->xvel;
					}
				}
			}
			//--- That's all the collision code, whew
		}
		else {
			break; // Nothing left to do when xvel and yvel are both 0
		}

		pb->frictiontimer -= settings.ships->SoccerBallFriction;

		if (pb->frictiontimer < 0) {
			pb->frictiontimer = 0;
		}

		updateticks--;
	}

	pb->updatetime = time;
	return collided;
}

#define TILE_IS_SOLID(type) (type >= vieNormalStart && type <= vieNormalEnd)
Host::IntersectResult Host::ballPathIntersect(int x0, int y0, int x1, int y1) {
	IntersectResult noIntersect = {};
	noIntersect.intersected = false;

	double dx = abs(x1 - x0);
	double dy = abs(y1 - y0);

	if (dx == 0 && dy == 0)
		return noIntersect;

	int x = x0 / 16000;
	int y = y0 / 16000;

	int n = 1;
	int xIncrement, yIncrement;
	double error = 0.f;

	if (dx == 0) {
		xIncrement = 0;
		error = std::numeric_limits<double>::infinity();
	}
	else if (x1 > x0) {
		xIncrement = 1;
		n += (x1 / 16000) - x;
		error = (x + 1 - (x0 / 16000.f)) * dy;
	}
	else {
		xIncrement = -1;
		n += x - (x1 / 16000);
		error = ((x0 / 16000.f) - x) * dy;
	}

	if (dy == 0) {
		yIncrement = 0;
		error = -std::numeric_limits<double>::infinity();
	}
	else if (y1 > y0) {
		yIncrement = 1;
		n += (y1 / 16000) - y;
		error -= (y + 1 - (y0 / 16000.f)) * dx;
	}
	else {
		yIncrement = -1;
		n += y - (y1 / 16000);
		error -= ((y0 / 16000.f) - y) * dx;
	}

	bool startingTile = true; // Ignore collisions occuring when the initial x0, y0 are inside a tile already
	IntersectResult result = {};
	result.ix = 0;
	result.iy = 0;
	while (n > 0) {
		if (!startingTile && TILE_IS_SOLID(map[getLinear(x, y)])) {
			result.intersected = true;
			return result;
		}

		if (error > 0) {
			y += yIncrement;
			error -= dx;

			result.iy = 16000 * y;
			if (yIncrement < 0)
				result.iy += 16000;
			double slopeX = (x1 - x0) / static_cast<double>(y1 - y0);
			result.ix = x0 + (result.iy - y0) * slopeX;
			result.dir = IntersectResult::Direction::Vertical;
		}
		else {
			x += xIncrement;
			error += dy;

			result.ix = 16000 * x;
			if (xIncrement < 0)
				result.ix += 16000;
			double slopeY = (y1 - y0) / static_cast<double>(x1 - x0);
			result.iy = y0 + (result.ix - x0) * slopeY;
			result.dir = IntersectResult::Direction::Horizontal;
		}

		startingTile = false;
		n--;
	}

	return noIntersect;
}